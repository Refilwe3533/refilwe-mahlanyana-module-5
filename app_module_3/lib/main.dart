import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:app_module_3/login_view.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:firebase_core/firebase_core.dart';
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    // Replace with actual values
    options: const FirebaseOptions(
      apiKey: "AIzaSyCVLKdzSadGYncgUGGBNcIfMiiQ8WdkCSY",
      authDomain: "mtnmodule5.firebaseapp.com",
      projectId: "mtnmodule5",
      storageBucket: "mtnmodule5.appspot.com",
      messagingSenderId: "1070107913176",
      appId: "1:1070107913176:web:7c26c49b84f5efccf820ad"
    ),
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Dramatique',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          floatingActionButtonTheme: const FloatingActionButtonThemeData(
            backgroundColor: Colors.pinkAccent,
          ),
          primarySwatch: Colors.pink,
            ),
        home: AnimatedSplashScreen(
            duration: 3000,
            splash:Lottie.asset("assets/pride.json"),
            nextScreen: LoginView(),
            splashTransition: SplashTransition.fadeTransition,
            backgroundColor: Colors.white));
  }
}

