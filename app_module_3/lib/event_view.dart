import 'package:app_module_3/dashbord_view.dart';
import 'package:app_module_3/event_list_view.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'model/event_model.dart';

class EventView extends StatefulWidget {
  const EventView({Key? key}) : super(key: key);

  @override
  _EventViewState createState() => _EventViewState();
}

class _EventViewState extends State<EventView> {
final eventNameController=TextEditingController();
final eventDateController=TextEditingController();
final eventLocationController=TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:const Text("Add Event"),),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 50,
            ),
            const Text("Event Name"),
            TextField(controller: eventNameController,),
            const SizedBox(
              height: 20,
            ),
            const Text("Location"),
             TextField(controller: eventLocationController,),
            const SizedBox(
              height: 20,
            ),
            const Text("Date"),
             TextField(controller: eventDateController,),
            const SizedBox(
              height: 50,
            ),
           Row(
             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
             children: [
               ConstrainedBox(
                 constraints: const BoxConstraints.tightFor(width: 250,height: 60),
                 child: ElevatedButton(
                     onPressed: (){
                       createEvent();
                       Navigator.push(
                         context,
                         MaterialPageRoute(builder: (context) => const EventListView()),
                       );
                     },
                     child: const Text("Add Event",style: TextStyle(color: Colors.white,fontSize: 17.0,fontWeight: FontWeight.bold),),
                     style: ElevatedButton.styleFrom(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)))
                 ),
               ),
               
             ],
           ),
          ],
        ),
      ),
    );
  }

void createEvent() async{
final db = FirebaseFirestore.instance.collection("events").doc();

	final event = Event(
    id: db.id ,
		eventName: eventNameController.text,
		location: eventLocationController.text,
		date: eventDateController.text
	);

	final value = event.toJson();

	await db.set(value);
}

}

