import 'package:app_module_3/dashbord_view.dart';
import 'package:app_module_3/signup_view.dart';
import 'package:flutter/material.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:const Text("Login"),),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 50,
            ),
            const Text("Name/Username"),
            const TextField(),
            const SizedBox(
              height: 20,
            ),
            const Text("Password"),
            const TextField(),
            const SizedBox(
              height: 50,
            ),
           Row(
             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
             children: [
               ConstrainedBox(
                 constraints: const BoxConstraints.tightFor(width: 250,height: 60),
                 child: ElevatedButton(
                     onPressed: (){
                       Navigator.push(
                         context,
                         MaterialPageRoute(builder: (context) => const DashboardView()),
                       );
                     },
                     child: const Text("Login",style: TextStyle(color: Colors.white,fontSize: 17.0,fontWeight: FontWeight.bold),),
                     style: ElevatedButton.styleFrom(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)))
                 ),
               ),
               ConstrainedBox(
                 constraints: const BoxConstraints.tightFor(width:250, height:60),
                 child: ElevatedButton(
                     onPressed: (){
                       Navigator.push(
                         context,
                         MaterialPageRoute(builder: (context) => const SignUpView()),
                       );
                     },
                     child: const Text("Sign Up",style: TextStyle(color: Colors.white,fontSize: 17.0,fontWeight: FontWeight.bold),),
                     style: ElevatedButton.styleFrom(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)))
                 ),
               ),
             ],
           ),
          ],
        ),
      ),
    );
  }
}

