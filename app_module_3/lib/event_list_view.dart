import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class EventListView extends StatefulWidget {
  const EventListView({Key? key}) : super(key: key);

  @override
  _EventListViewState createState() => _EventListViewState();
}

class _EventListViewState extends State<EventListView> {
  final nameController = TextEditingController();
  final dateController = TextEditingController();
  final locationController = TextEditingController();
  final db = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Events"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: StreamBuilder(
          stream: db.collection('events').snapshots(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            return ListView.builder(
              itemCount: snapshot.data?.docs.length,
              itemBuilder: (context, int index) {
                var evenList = snapshot.data.docs;
                return ListTile(
                  leading: const Icon(Icons.beach_access),
                  title: Text(evenList[index]['event_name']),
                  subtitle: Text(
                      "${evenList[index]['date']} location: ${evenList[index]['location']}"),
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return Dialog(
                            alignment: Alignment.center,
                            child: Padding(
                              padding: const EdgeInsets.all(24),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  const Text("Update Event",
                                      style: TextStyle(fontSize: 30)),
                                  TextField(
                                    controller: nameController,
                                    decoration: InputDecoration(
                                      border: const OutlineInputBorder(),
                                      labelText:
                                          "${evenList[index]['event_name']}",
                                    ),
                                  ),
                                  TextField(
                                    controller: locationController,
                                    decoration: InputDecoration(
                                        border: const OutlineInputBorder(),
                                        labelText:
                                            "${evenList[index]['location']}"),
                                  ),
                                  TextField(
                                    controller: dateController,
                                    decoration: InputDecoration(
                                        border: const OutlineInputBorder(),
                                        labelText:
                                            "${evenList[index]['date']}"),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      TextButton(
                                        style: ButtonStyle(
                                          foregroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.red),
                                        ),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        child: const Text(
                                          'Cancel',
                                          style: TextStyle(
                                              fontSize: 30,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      TextButton(
                                        style: ButtonStyle(
                                          foregroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.green),
                                        ),
                                        onPressed: () async {
                                          final db = FirebaseFirestore.instance
                                              .collection("events")
                                              .doc(evenList[index]["id"]);

                                          String name;
                                          String location;
                                          String date;

                                          if (nameController.text != '') {
                                            name = nameController.text;
                                          } else {
                                            name =
                                                evenList[index]['event_name'];
                                          }

                                          if (locationController.text != '') {
                                            location = locationController.text;
                                          } else {
                                            location =
                                                evenList[index]['location'];
                                          }

                                          if (dateController.text != '') {
                                            date = dateController.text;
                                          } else {
                                            date = evenList[index]['date'];
                                          }
                                          await db.update({
                                            'event_name': name,
                                            'location': location,
                                            'date': date,
                                          });
                                        },
                                        child: const Text(
                                          'Update',
                                          style: TextStyle(
                                              fontSize: 30,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          );
                        });
                  },
                  trailing: IconButton(
                    icon: const Icon(Icons.delete_outline, color: Colors.red),
                    onPressed: () {
                      db
                          .collection('events')
                          .doc(evenList[index]["id"])
                          .delete();
                    },
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
