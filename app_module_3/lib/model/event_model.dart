import 'dart:convert';

Event eventFromJson(String str) => Event.fromJson(json.decode(str));

String eventToJson(Event data) => json.encode(data.toJson());

class Event {
    Event({
        required this.id,
        required this.eventName,
        required this.location,
        required this.date,
    });

    String id;
    String eventName;
    String location;
    String date;

    factory Event.fromJson(Map<String, dynamic> json) => Event(
        eventName: json["event_name"],
        location: json["location"],
        date: json["date"], 
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "event_name": eventName,
        "location": location,
        "date": date,
        "id": id,
    };
}